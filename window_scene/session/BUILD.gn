# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("../../windowmanager_aafwk.gni")

config("session_public_config") {
  include_dirs = [
    "${window_base_path}/window_scene",
    "${window_base_path}/window_scene/intention_event/service/anr_manager/include",
    "${window_base_path}/window_scene/intention_event/framework/anr_handler/include",
    "${window_base_path}/window_scene/intention_event/utils/include",
    "${window_base_path}/interfaces/innerkits/dm",
    "${window_base_path}/wm/include/zidl",

    # for window_manager_hilog
    "${window_base_path}/utils/include",
    "${window_base_path}/window_scene/common/include",

    # for WMError Code
    "${window_base_path}/interfaces/innerkits/wm",
  ]
}

ohos_shared_library("scene_session") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    cfi_vcall_icall_only = true
    debug = false
  }
  sources = [
    "container/src/window_event_channel.cpp",
    "container/src/zidl/session_stage_proxy.cpp",
    "container/src/zidl/session_stage_stub.cpp",
    "container/src/zidl/window_event_channel_proxy.cpp",
    "container/src/zidl/window_event_channel_stub.cpp",
    "host/src/extension_session.cpp",
    "host/src/keyboard_session.cpp",
    "host/src/main_session.cpp",
    "host/src/move_drag_controller.cpp",
    "host/src/root_scene_session.cpp",
    "host/src/scb_system_session.cpp",
    "host/src/scene_persistence.cpp",
    "host/src/scene_persistent_storage.cpp",
    "host/src/scene_session.cpp",
    "host/src/session.cpp",
    "host/src/sub_session.cpp",
    "host/src/system_session.cpp",
    "host/src/zidl/session_proxy.cpp",
    "host/src/zidl/session_stub.cpp",
  ]

  public_configs = [ ":session_public_config" ]

  deps = [
    "${window_base_path}/dm:libdm",
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
    "${window_base_path}/window_scene/common:window_scene_common",
    "${window_base_path}/window_scene/intention_event/service:intention_event_anr_manager",
    "${window_base_path}/window_scene/interfaces/innerkits:libwsutils",
    "${window_base_path}/window_scene/screen_session_manager:screen_session_manager_client",
    "${window_base_path}/window_scene/session:screen_session",
  ]

  external_deps = [
    "ability_base:base",
    "ability_base:session_info",
    "ability_base:want",
    "ability_runtime:ability_start_setting",
    "ability_runtime:process_options",
    "c_utils:utils",
    "eventhandler:libeventhandler",
    "graphic_2d:librender_service_client",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "init:libbegetutil",
    "input:libmmi-client",
    "ipc:ipc_single",
    "preferences:native_preferences",
  ]
  public_external_deps = [
    "accessibility:accessibility_common",
    "accessibility:accessibility_interface",
    "input:libmmi-util",
  ]
  defines = []

  if (defined(global_parts_info) &&
      defined(global_parts_info.barrierfree_accessibility)) {
    external_deps += [ "accessibility:accessibility_common" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.powermgr_power_manager)) {
    external_deps += [ "power_manager:powermgr_client" ]
    defines += [ "POWER_MANAGER_ENABLE" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.resourceschedule_soc_perf)) {
    external_deps += [ "soc_perf:socperf_client" ]
    defines += [ "SOC_PERF_ENABLE" ]
  }

  if (!defined(global_parts_info) ||
      defined(global_parts_info.inputmethod_imf)) {
    imf_enable = true
  } else {
    imf_enable = false
  }
  print("imf_enable: ", imf_enable)
  if (imf_enable == true) {
    external_deps += [ "imf:inputmethod_client" ]
    defines += [ "IMF_ENABLE" ]
  }

  innerapi_tags = [ "platformsdk" ]
  part_name = "window_manager"
  subsystem_name = "window"
}

ohos_shared_library("screen_session") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    cfi_vcall_icall_only = true
    debug = false
  }
  sources = [
    "screen/src/screen_property.cpp",
    "screen/src/screen_session.cpp",
  ]

  public_configs = [ ":session_public_config" ]

  deps = [
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
  ]

  external_deps = [
    "c_utils:utils",
    "graphic_2d:librender_service_client",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "init:libbegetutil",
  ]
  public_external_deps = [
    "accessibility:accessibility_common",
    "input:libmmi-util",
  ]

  innerapi_tags = [ "platformsdk" ]
  part_name = "window_manager"
  subsystem_name = "window"
}
